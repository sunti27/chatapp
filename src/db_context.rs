use sqlx::postgres::{PgPool, PgPoolOptions};

use std::time::Duration;

pub(crate) async fn get_pool() -> PgPool {
    let db_connection_str = dotenv::var("DATABASE_URL").unwrap();

    PgPoolOptions::new()
        .max_connections(5)
        .connect_timeout(Duration::from_secs(3))
        .connect(&db_connection_str)
        .await
        .expect("Cannot connect to database")
}