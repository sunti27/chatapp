
use std::{
    fs::File,
    sync::Arc
};

use chrono::Utc;

use tracing_subscriber::{
    layer::{Layer, SubscriberExt},
    filter,
    util::SubscriberInitExt,
};

use axum::{
    body::Body,
    http::{Request, StatusCode}, 
    middleware::Next, response::IntoResponse
};

pub(crate) fn setup() {
    let log_file = File::create("./logs/logs.log").expect("Cannot create log-file");

    let filter = filter::filter_fn(|metadata| {
        metadata.target().starts_with(env!("CARGO_PKG_NAME"))
    });

    let layer = tracing_subscriber::fmt::layer()
        .with_writer(Arc::new(log_file))
        .with_ansi(false)
        .compact()
        .with_line_number(true)
        .with_filter(filter);

    tracing_subscriber::registry()
        .with(layer)
        .init();
}

pub(crate) async fn log_req_res(
    req: Request<Body>,
    next: Next<Body>,
) -> Result<impl IntoResponse, (StatusCode, String)> {
    // [200] 2022.05.27 16:19:38:665 -> 2022.05.27 16:19:38:681    GET /api/users
    
    let method = req.method().clone();
    let endpoint = req.uri().to_string();
    let start_time = Utc::now().format("%Y.%m.%d %H:%M:%S").to_string();

    let res = next.run(req).await;

    let status_code = res.status().as_u16();
    let end_time = Utc::now().format("%Y.%m.%d %H:%M:%S").to_string();

    tracing::info!("[{:?}] {:?} -> {:?} {:?} {:?}",
        status_code,
        start_time = start_time.as_str(),
        end_time = end_time.as_str(),
        method = method.as_str(),
        endpoint = endpoint.as_str(),
    );

    Ok(res)
}