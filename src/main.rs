mod logging;
mod db_context;

use axum::{
    routing::get,
    Router, Extension, http::StatusCode, middleware
};
use sqlx::postgres::PgPool;

use std::net::SocketAddr;

#[tokio::main]
async fn main() {
    dotenv::dotenv().expect("Could not load .env-file");    

    logging::setup();

    let app = Router::new()
        .route("/", get(root))
        .layer(middleware::from_fn(logging::log_req_res))
        .layer(Extension(db_context::get_pool().await));
        

    let address = SocketAddr::from(([127, 0, 0, 1], 3000));

    println!("Server listening on http://{address}");

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .expect("Server failed");
}

async fn root(Extension(pool): Extension<PgPool>) -> Result<String, (StatusCode, String)> {
    sqlx::query_scalar("select 'hello world from pg'")
        .fetch_one(&pool)
        .await
        .map_err(|err| (StatusCode::INTERNAL_SERVER_ERROR, err.to_string()))
}